//
//  ViewController.swift
//  Shadow2View
//
//  Created by EPITADMBP04 on 4/7/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var shadowview : UIView!
    @IBOutlet weak var button1 : UIButton!
    
    @IBOutlet weak var shadowview1 : UIView!
    @IBOutlet weak var button2 : UIButton!
    
    @IBOutlet weak var shadowview2: UIView!
    @IBOutlet weak var button3 : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  //----------------------------------------------------------------------------------------------------
     /*   // Creating Button Programatically. Commenting the code i have buttons in storyboard.
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        button.layer.cornerRadius = 12
        button.backgroundColor = UIColor.init(red: 48/255, green: 155/255, blue: 255/255, alpha: 1)
        button.setTitle("Whats goodie?!", for: .normal)
        
        let constraints = [button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
                           button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                           button.widthAnchor.constraint(equalToConstant: 200),
                           button.heightAnchor.constraint(equalToConstant: 60),]
        NSLayoutConstraint.activate(constraints)  */
 //----------------------------------------------------------------------------------------------------
        shadowview.layer.cornerRadius = 10.0
        shadowview.layer.shadowColor = UIColor.white.cgColor
        shadowview.layer.shadowOffset = .zero
        shadowview.layer.shadowOpacity = 0.6
        shadowview.layer.shadowRadius = 15.0
        shadowview.layer.shadowPath = UIBezierPath(rect: shadowview.bounds).cgPath
        shadowview.layer.shouldRasterize = true
        
        button1.layer.cornerRadius = 10.0
        button1.layer.shadowColor = UIColor.white.cgColor
        button1.layer.shadowOffset = .zero
        button1.layer.shadowOpacity = 0.6
        button1.layer.shadowRadius = 15.0
        
        shadowview1.layer.cornerRadius = 10.0
        shadowview1.layer.shadowColor = UIColor.white.cgColor
        shadowview1.layer.shadowOffset = .zero
        shadowview1.layer.shadowOpacity = 0.6
        shadowview1.layer.shadowRadius = 15.0
        shadowview1.layer.shadowPath = UIBezierPath(rect: shadowview1.bounds).cgPath
        shadowview1.layer.shouldRasterize = true
        
        button2.layer.cornerRadius = 10.0
        button2.layer.shadowColor = UIColor.white.cgColor
        button2.layer.shadowOffset = .zero
        button2.layer.shadowOpacity = 0.6
        button2.layer.shadowRadius = 15.0
        
        shadowview2.layer.cornerRadius = 10.0
        shadowview2.layer.shadowColor = UIColor.white.cgColor
        shadowview2.layer.shadowOffset = .zero
        shadowview2.layer.shadowOpacity = 0.6
        shadowview2.layer.shadowRadius = 15.0
        shadowview2.layer.shadowPath = UIBezierPath(rect: shadowview2.bounds).cgPath
        shadowview2.layer.shouldRasterize = true
    
        button3.layer.cornerRadius = 10.0
        button3.layer.shadowColor = UIColor.white.cgColor
        button3.layer.shadowOffset = .zero
        button3.layer.shadowOpacity = 0.6
        button3.layer.shadowRadius = 15.0
        
        button1.addTarget(self, action: #selector(self.whatsGoodieButton(sender:)), for: .touchUpInside)
        button2.addTarget(self, action: #selector(self.whatsGoodieButton(sender:)), for: .touchUpInside)
        button3.addTarget(self, action: #selector(self.whatsGoodieButton(sender:)), for: .touchUpInside)
    }
    
    @objc fileprivate func whatsGoodieButton(sender: UIButton) {
        print("whats goodie guys & gals?!")
        self.animateView(sender)
    }
    
    fileprivate func animateView(_ viewToAnimate: UIView) {
        UIView.animate(withDuration: 0.15, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
            viewToAnimate.transform = CGAffineTransform(scaleX: 0.92, y: 0.92)
        }) { (_) in
        UIView.animate(withDuration: 0.15, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 2, options: .curveEaseIn, animations: {
        viewToAnimate.transform = CGAffineTransform(scaleX: 1, y: 1)
    }, completion: nil)
        }
    }
}

        
       


























